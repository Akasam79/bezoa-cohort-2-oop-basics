﻿ using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Globalization;

namespace OOP
{
    public static class Application
    {
        public static void Run()
        {
            var menu = new StringBuilder();
            menu.Append("Hello, welcome to my App:\n");
            menu.AppendLine("Enter your FirstName");
            Console.WriteLine(menu.ToString());

            var firstName = Console.ReadLine();
           while(string.IsNullOrWhiteSpace(firstName))
                {
                    Console.WriteLine("firstName Cannot be empty");
                    Console.WriteLine("Enter your FirstName");
                    firstName = Console.ReadLine();
                }

            Console.WriteLine("Enter lastName");
            var lastName = Console.ReadLine();
            while(string.IsNullOrWhiteSpace(lastName))
            {
                Console.WriteLine("lastName must not be empty");
                    Console.WriteLine("Enter lastName");
                    lastName = Console.ReadLine();
            }

            Console.WriteLine("Enter email");
            var email = Console.ReadLine();
            bool IsValidEmail()
            {
                try {
                    var mail = new System.Net.Mail.MailAddress(email);
                    return true;
                }
                catch {
                    return false;
                }
            }
            while(string.IsNullOrWhiteSpace(email))
            {
                Console.WriteLine("email must not be empty");
                Console.WriteLine("Enter email");
                email = Console.ReadLine();
            }
            while(IsValidEmail().Equals(false))
            {
                Console.WriteLine("please enter a valid email address");
                email = Console.ReadLine();
            }
            Console.WriteLine("Enter birthday");
            var birthday = Console.ReadLine();
            
            while(!isValidDate(birthday))
            {
                Console.WriteLine("pls enter a valid date");
                birthday = Console.ReadLine();
            }
            

            Console.WriteLine("Select your Gender: \n 1. male \n 2. Female \n 3. Prefer not to say");
            var gender = Console.ReadLine();
            while(gender != "1" && gender != "2" && gender != "3")
            {
                Console.WriteLine("Invalid Gender Selection");
                Console.WriteLine("Select your Gender: \n 1. male \n 2. Female \n 3. Prefer not to say");
                gender = Console.ReadLine();
            }
            var selectedGender = GenderSelection(gender);       

            

            Console.WriteLine("Enter password");
            var password = Console.ReadLine();
            
            while(string.IsNullOrWhiteSpace(password))
            {
                Console.WriteLine("password cannot be empty");
                    Console.WriteLine("Enter password");
                    password = Console.ReadLine();
            }
           
            Console.WriteLine("Confirm Password");
            var confirmPassword = Console.ReadLine();

            while(string.IsNullOrWhiteSpace(confirmPassword) )
            {
                Console.WriteLine("field must not be empty");
                    Console.WriteLine("Confirm Password");
                    confirmPassword = Console.ReadLine();
            }
            while(!string.Equals(password, confirmPassword))
            {
                Console.WriteLine("passwords do not match");
                    Console.WriteLine("Confirm Password");
                    confirmPassword = Console.ReadLine();
            }

            
            while(string.Equals(password, confirmPassword)) {

                try{
                    var formData = new Register { 
                    FirstName = firstName, 
                    LastName = lastName,
                    Email = email,
                    Birthday = DateTime.Parse(birthday, new System.Globalization.CultureInfo("pt-BR")),
                    Password = password,
                    ConfirmPassword = confirmPassword,
                    Gender = selectedGender };

                    AccountService.Register(formData);
                }
                catch(FormatException ex)
                {
                    Console.WriteLine("Registration failed: " + ex.Message);
                }
                Console.ReadLine();
        }
            }

        public static Gender GenderSelection(string gender)
        {
            
            switch (gender)
            {
                case "1":
                    return Gender.Male;       
                case "2":
                    return Gender.Female;
                case "3":
                    return Gender.PreferNotToSay;
                default:
                    return Gender.SelectGender;
            }
        }
         private static bool isValidDate(string date)
        {
            string[] formats= {"M/d/yyyy", "M/d/yyyy", 
                   "MM/dd/yyyy", "M/d/yyyy", 
                   "M/d/yyyy", "M/d/yyyy", 
                   "M/d/yyyy", "M/d/yyyy", 
                   "MM/dd/yyyy", "M/dd/yyyy", 
                    "dd/MM/yyyy", "yyyy/MM/dd"};
           DateTime result;
           if(!DateTime.TryParseExact(date, formats, 
                              new CultureInfo("en-US"), 
                              DateTimeStyles.None, 
                              out result) || string.IsNullOrWhiteSpace(date))
            {
                return false;
            }
            else
            {
                return true;
            }
            
        }

        

        }
}
